import sys

def readthesamFALSEG(fin):
	"""
	mappingcondition : a string that describe the mapping parameter (strain, relaxed)
	fin : a SAM file to test
	"""
	f=open(fin,'r')
	out=[0,0,0]
	l=f.readline()
	l=f.readline()
	l=f.readline()
	l=f.readline()
	while l:
		ls=l.split()
		v=int(ls[3])
		if v!=0:
			#print(v)
			if v>8650:
				out[2]+=1
			elif v<4483: #H1.1
				out[0]+=1
			else : # H1.2
				out[1]+=1
		l=f.readline()
		#l=f.readline()
	f.close()
	print(out)
	
def readthesamTAIR10(fin):
	"""
	mappingcondition : a string that describe the mapping parameter (strain, relaxed)
	fin : a SAM file to test
	"""
	f=open(fin,'r')
	out=[0,0,0]
	l=f.readline()
	l=f.readline()
	l=f.readline()
	l=f.readline()
	l=f.readline()
	l=f.readline()
	l=f.readline()
	l=f.readline()
	while l:
		ls=l.split()
		v=int(ls[3])
		if v!=0:
			if v>7846366 and v<7846517 and ls[2]=='2':
				out[2]+=1
			elif v>2077562 and v<2077658 and ls[2]=='1': #H1.1
				out[0]+=1
			elif v>7846366 and v<7846517 and ls[2]=='2' : # H1.2
				out[1]+=1
		l=f.readline()
		#l=f.readline()
	f.close()
	print(out)
	
	

if sys.argv[1]=='False':
	readthesamFALSEG(sys.argv[2])
else :
	readthesamTAIR10(sys.argv[2])
