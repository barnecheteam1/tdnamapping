#!/bin/sh

firstread=$1
bowtierep='/home/carron/Documents/bowtie2-2.3.5.1-linux-x86_64/indexes/AT' #if needed to indicate bowtie2 location if upgrade
indexrep='Lerindex/LER'



bowtie2 -x $indexrep --very-sensitive -p 10 -U $firstread > tampon.sam
python3 findmutant.py 'False' tampon.sam >$1.result.txt
bowtie2 -x $indexrep --very-fast-local -p 10 -U $firstread > tampon.sam
python3 findmutant.py 'False' tampon.sam >>$1.result.txt

bowtie2 -x $bowtierep --very-sensitive -p 10 -U $firstread > tampon.sam
python3 findmutant.py 'True' tampon.sam >>$1.result.txt
bowtie2 -x $bowtierep --very-fast-local -p 10 -U $firstread > tampon.sam
python3 findmutant.py 'True' tampon.sam >>$1.result.txt
rm tampon.sam

sed 's/.//;s/.$//' $1.result.txt >$1.txt
rm $1.result.txt

Rscript tobarplot.R $1.txt $1.png
